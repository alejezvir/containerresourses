# ContainerResourses

Works on ubuntu 18.04 with ansible installed

This playbook install docker-ce first.
Then it build\pull images and runs 2 containers
on the local machine.

First one is nginx with ssh daemon.
Second is uwsgi web app wrote with python *flask*
module and ssh daemon.

Nginx works in reverse proxy mode and use proxypass to 
connet to uwsgi server. *Flask* uses *psutil* module to
collect information about the host system and */sys/fs*
info to find out container resourses. If you refresh
web page service will display actual info about the system.


In case of pull it starts much faster then build

To pull images use:

$sudo ansible-playbook ansible-playbookpull.yml

To build images use:

$sudo ansible-playbook ansible-playbookbuild.yml

web container runs with 512m and 0 cpuid

Then visit *http://localhost:7080* and see the result.


$ssh root@localhost -p7022
to connect to nginx container

$ssh root@localhost -p8022
to connect to web container

