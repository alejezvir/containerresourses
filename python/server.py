from flask import Flask, render_template

from collections import namedtuple
import psutil

app = Flask(__name__)


Message = namedtuple("Message", "text")
messages = [] 

@app.route('/hwinfo')
def hwinfo():

    f_cpu = open('/sys/fs/cgroup/cpuset/cpuset.cpus')
    line_cpus = f_cpu.readline()

    f_lim_mem = open('/sys/fs/cgroup/memory/memory.limit_in_bytes')
    line_lim_mem = f_lim_mem.readline()

    f_av_mem = open('/sys/fs/cgroup/memory/memory.usage_in_bytes')
    line_av_mem = f_av_mem.readline()

    

    cpus = 'Number of CPUs on the host: ' + str(psutil.cpu_count())
    loadav = 'Load average on the host: ' + str(psutil.getloadavg())
    mem = "Memory on the host: " + str(psutil.virtual_memory())
    swap = "Swap on the host: " + str(psutil.swap_memory())
    disk = "Disk usage in the container: " + str(psutil.disk_usage('/'))
    iops = "IOPS stat on the host: " + str(psutil.disk_io_counters(perdisk=False))
    
    messages.append(Message("----------------------START REPORT----------------------"))
    messages.append(Message("Available CPU ids in the container: " + line_cpus))
    messages.append(Message("Available memory in the container: " + line_lim_mem))
    messages.append(Message("Usage memory in the container: " + line_av_mem))
    messages.append(Message(disk))
    messages.append(Message(cpus))
    messages.append(Message(loadav))
    messages.append(Message(mem))
    messages.append(Message(swap))
    messages.append(Message(iops))
    messages.append(Message("------------------------END REPORT-----------------------"))

    return render_template('index.html', messages=messages)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
